---
layout: post
title: "Todo acerca de Tor (Español)"
date: 2021-10-01 10:00:00 -500
file: https://guardianproject.info/podcast-audio/AllAboutTorEspanol.mp3
summary: "En este episodio Gaba y Meskio del equipo de Tor nos acompañan en una platica casual acerca de la red de Tor"
description: "En este episodio Gaba y Meskio del equipo de Tor nos acompañan en una platica casual acerca de la red de Tor"
duration: "37:49" 
length: "51713321"
explicit: "no" 
keywords: "Tor, Internet Safety, Privacy, Annonymity, Orbot, Tor Browser, Onion Browser"
block: "no" 
voices: "Fabiola Maurice, Gaba, Meskio"

---

### About this Episode

En este episodio Gaba y Meskio del equipo de Tor nos acompañan en una platica casual acerca de la red de Tor y todas las funciones nuevas disponibles para los usuarios, como los Puentes, Transportes Conectables,  y OnionShare,  también hablamos acerca de la desinformación difundida por algunos medios acerca de quienes están detrás de Tor y quienes lo usan.  Y podras enterarte de las muchas maneras en que puedes contribuir a Tor y ser parte de las increible comunidad, de desarrolladores,  voluntarios, activistas y defensores de derechos humanos que trabajan en conjuto para crear una experiancia mas segura al usar el internet.
