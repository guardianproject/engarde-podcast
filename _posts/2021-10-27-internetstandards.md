---
layout: post
title: "Internet Standards for Internet Freedom"
date: 2021-10-27 10:00:00 -400
file: https://guardianproject.info/podcast-audio/internetstandards-oct2021.mp3
description: "Learn more about how Internet standards are developed, and how they impact Internet freedom and privacy"
summary: "Learn more about how Internet standards are developed, and how they impact Internet freedom and privacy"
duration: "37:05" 
length: "56929421"
explicit: "no" 
keywords: "standards, internet, IETF, pluggable transports"
block: "no" 
voices: "Fabiola and David of Guardian Project"

---

### About this Episode

In this episode, David Oliver, one of the Guardian Project founders, joins us to share with the Internet Freedom Community the importance of the Internet Engineering Task Force (IETF) to the topics of privacy and security. Listening to this podcast, you will learn what the IETF is, how is it organized, how Internet standards are produced, and the most important challenges o Internet Freedom facing the IETF today.

### Show Notes and Links

* David is at [david@guardianproject.info](mailto:david@guardianproject.info) 
* IETF [ietf.org](https://www.ietf.org/)
* Pluggable Transports: [pluggabletransports.info](https://pluggabletransports.info)
* "The MASQUE Protocol - IETF Standards Work Related to Pluggable Transports" [https://www.pluggabletransports.info/blog/ptim2020masque/](https://www.pluggabletransports.info/blog/ptim2020masque/)

Arc by Metre is licensed under a Attribution-NonCommercial 4.0 International License and is available [here](https://freemusicarchive.org/music/Metre/surface-area/arc)

