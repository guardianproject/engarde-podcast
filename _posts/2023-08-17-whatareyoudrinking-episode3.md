---
layout: post
title: "What Are You Drinking?! Episode #3 - Intel GPU #DataFail"
date: 2023-09-11 12:30:00 -500
file: https://guardianproject.info/podcast-audio/WhatareyoudrinkingCleanInsightsPodcast3.mp3
description: "The Clean Insights Weekly Podcast - Episode 3"
summary: "The Clean Insights Weekly Podcast - Episode 3"
duration: 34957204
length: "36:25"
explicit: "no" 
keywords: "cleaninsights, analytics, privacy"
block: "no" 
voices: "John, Tiff, Nathan, Alfred"

---

### About this Episode

On today's episode, we again play "Explain It" - a game that helps us find ways to help different people from different walks of life understand why they might care about Clean Insights. We ALSO talk about how Intel #DATAFAIL'd in a big way with their new GPU usage tracking software. There is some controversy, so listen to us figure out how Clean Insights could have helped Intel #WIN

Come join us to hear more about our work on privacy-preserving measurement and respectful analytics with https://cleaninsights.org

A video podcast version is also available at [https://www.youtube.com/watch?v=lCNQF7JmqRs](https://www.youtube.com/watch?v=lCNQF7JmqRs)

### Show Notes and Links

- [Clean Insights Website](https://cleaninsights.org)
- [Clean Insights Docs](https://docs.cleaninsights.org)
- [Get Your Insightful Beans!](https://cleaninsights.org/beans)
- [Guardian Project contact info](https://guardianproject.info/contact/)
