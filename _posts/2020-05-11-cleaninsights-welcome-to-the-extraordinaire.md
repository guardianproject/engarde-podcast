---
layout: post
title: "Clean Insights: Welcome to the Symposium Extraordinaire"
date: 2020-05-11 10:30:00 -500
file: https://guardianproject.info/podcast-audio/engarde-cleaninsights-podcast1.mp3
summary: "The Clean Insights Ringleaders Welcome You to the Symposium Extraordinaire!"
description: "The Clean Insights Ringleaders Welcome You to the Symposium Extraordinaire!"
duration: "14:21" 
length: "11521416"
explicit: "no" 
keywords: "clean insights, privacy preserving measurement, ethical analytics, open-source"
block: "no" 
voices: "Nathan Freitas (n8fr8), Carrie Winfrey (Okthanks), Tiffany Robertson (Okthanks), Gina Helfrich (BASICS - Internews)"
---

### About this Episode

The Clean Insights Ringleaders (Nathan of [Guardian Project](https://guardianproject.info), Gina of [Internews Basic](https://globaltech.internews.org/our-resources/basics), Carrie and Tiffany of [Okthanks](https://okthanks.com)) welcome you to the opening week of the [Symposium Extraordinaire!](https://cleaninsights.org/event)

#### Carrie Winfrey, Okthanks 
Founder of Okthanks, Design lead at Guardian Project
An Interaction Designer by trade, Carrie works hand-in-hand with teams to craft clear, effective user experiences and brand messages. She has over 8 years of experience specializing in User Experience (UX) and User Interface (UI) design for mobile and web-based applications. She has worked with multiple startups to name their products and services, and to produce strong visual brand identities.

#### Gina Helfrich, Internews
Dr. Gina Helfrich is Program Officer for Global Technology at Internews.
Previously, she served as Director of Communications and Culture at NumFOCUS, a non-profit that supports better science through open code. Gina is an accomplished and visionary leader with a track record of success across a variety of fields, including nonprofits, higher education, and business. She was co-founder of recruitHER, a women-owned recruiting & consulting firm committed to making the tech industry more inclusive and diverse. The brand strategy Gina developed and executed for recruitHER quickly earned national attention and a list of high-profile clients including Pandora, GitHub, Pinterest, and RunKeeper. She earned press features including stories in both Austin Monthly and Austin Woman magazines, a talk at SXSW, and an interview on the Stuff Mom Never Told You podcast.

#### Nathan Freitas, Guardian Project
Nathan is the founder and director of Guardian Project, an award-winning, open-source, mobile security collaborative with millions of users and beneficiaries worldwide. Their most well known app is Orbot, which brings the Tor anonymity and circumvention network to Android devices, and has been installed more than 20 million times. In late 2017, he co-designed with Edward Snowden, an app called Haven, which works as a personal security system that puts the power of surveillance back into the hands of the most vulnerable and under threat.
His work on off-grid, decentralized, secure mobile communication networks, dubbed Wind, was originally imagined and workshopped while a fellow at the Berkman-Klein Center in 2015. In 2018, Wind was selected as a finalist in the Mozilla-National Science Foundation "Wireless Innovation for a Networked Society (WINS)" Challenges.

#### Tiffany Robertson, Community Ambassador, Okthanks
Tiffany is committed to justice. Working around the globe, she has accumulated diverse experiences that contribute to her effort to build awareness and support around social justice issues.
In Rwanda, Tiffany worked to empower women to build sustainable businesses with Keza, an ethical fashion venture. In London, she produced marketing profiles, improved member relations and helped coordinate the SOURCE Summit for the Ethical Forum Fashion. In Tennessee, she brightened days as a barista and social media marketing assistant with Sunnyside, a startup drive-thru coffee hut. In Ningbo, China, she taught English as a second language. In Alaska, Tiffany led visitors through the state’s terrain as a driver guide. Through each experience, Tiffany works with a purpose to understand how to develop better ways to communicate unfair issues to build support around social justice by companies and consumers.

Music courtesy of Archive.org: [Here Comes The Circus](http://archive.org/details/HereComesTheCircus)

### Show Notes and Links

- [Clean Insights Symposium Extraordinaire](https://cleaninsights.org/event)
- [Clean Insights Presentation - February 2020](https://gitlab.com/cleaninsights/clean-insights-android-sdk/-/blob/master/docs/Clean%20Insights%20-%20%20Feb%202020.pdf)

