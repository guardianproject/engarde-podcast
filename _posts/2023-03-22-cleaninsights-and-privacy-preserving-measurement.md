---
layout: post
title: "Clean Insights and Privacy Preserving Measurement"
date: 2023-03-22 12:30:00 -500
file: https://guardianproject.info/podcast-audio/cleaninsightsoverviewmarch2023.mp3
description: "Introduction to Privacy Preserving Measurement and Clean Insights"
summary: "Introduction to Privacy Preserving Measurement and Clean Insights"
duration: "34:28"
length: 32947366 
explicit: "no" 
keywords: "cleaninsights, privacy, analytics"
block: "no" 
voices: "John"

---

### About this Episode

Let's talk about privacy preserving measurement and how Clean Insights fits into that landscape.  We'll talk about all the various kinds of privacy folks might want then explain the techniques for acheiving them in plain English.

View and/or download the slides from this talk through the [Clean Insights Design Talks on Gitlab](https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/talks/Clean%20Insights%20%26%20Privacy%20Preserving%20Measurement%202023.pdf)

A video version is [also available on youtube](https://www.youtube.com/watch?v=8YgCuCBuYH8)

### Show Notes and Links

- [Clean Insights Website](https://cleaninsights.org)
- [Guardian Project contact info](https://guardianproject.info/contact/)
