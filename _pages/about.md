---
layout: page
title: "About Guardian Project"
permalink: /about
---

While smartphones have been heralded as the coming of the next generation of communication and collaboration, they are a step backwards when it comes to personal security, anonymity and privacy.

Guardian Project creates easy to use secure apps, open-source software libraries, and customized solutions that can be used around the world by any person looking to protect their communications and personal data from unjust intrusion, interception and monitoring.

Whether your are an average person looking to affirm your rights or an activist, journalist or humanitarian organization looking to safeguard your work in this age of perilous global communication, we can help address the threats you face.

Learn more on our website at [https://guardianproject.info](https://guardianproject.info)

### How we do it

If you're curious about how this site and the podcast works you can [find out more here on this page]({{ "/podcast.html" | prepend: site.baseurl }}).
